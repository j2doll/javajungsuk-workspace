// OperatorEx06.java

class OperatorEx06 { 
      public static void main(String[] args) { 
            byte a = 10; 
            byte b = 20;
            
            // byte c = a + b; // casting error (java 12)
            byte c = (byte)(a + b);

            System.out.println(c); 
      } 
} 
